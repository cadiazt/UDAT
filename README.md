# Interfaz web UDAT

La interfaz web consta de un mapa en pantalla completa que muestra las UDAT, Reconectadores y Subestaciones de la CGE de forma geo referenciada, estam implementación esta pensada para ser embebida en iFix. 
Cada item en el mapa permite la apertura de su  "picture"  correspondiente además de mostrar en tiempo real las alarmas de UDAT reconectadores.

La implementación de la comunicación entre Ifix y la interfaz web se realiza mediante OPC. 

    - En el lado de la pagina web al hacer un click sobre un item se envía el nombre del item mediante OPC a la PDB del iFix.
    - En el lado del servidor iFix se catpa el evento de que ha cambiado el valor del tag y se abre el picture correspondiente a ese item.

La programación se desarrolla en visual studio 2017

## Front end.

        Trobjetos.asxp:
        
###         Header:       
                    - Implementa Mabox version 3.1.1 : Permite insertar el mapa en la pagina web (Ejemplos y tutoriales https://www.mapbox.com/mapbox.js/api/v3.1.1/)
                    - Implementa Leaflet version 1.0.0 : Permite la agrupación de items en clusters y la modificacion de los simbolos asociados a cada cluster, además de multiples funcionalidades (Ejemplos y tutoriales https://github.com/Leaflet/Leaflet.markercluster)
                    - Implementa Jquery version 1.3.2
                       
                       
###         Body:          
                 Se llama a la funcion CargarMapa() al inicializar la pagina, mediante "onload" y se abre en el "div" con el "id = map"
                 
                        <body onload="CargarMapa()">
                            <form id="form1" runat="server">
                                <div id='map'></div>
                            </form>
                        </body>   
                
                La funcion CargarMapa() 
                
                Al usar Mapbox se debe tener un accsessToken que se obtine al crear una cuenta, es personal y asocia el tipo de cuenta y cantidad de visualizaciones permitidas de forma concurrente, en la version gratuita se tiene:
                
                    - 50,000 map views / mo
                    - 50,000 geocode requests / mo
                    - 50,000 directions requests / mo
                       
                Se utilizan 4 Clusters para agrupar los items, 
                    
                    - markersAlarms : Agrupa todos los Udat y reconectadores que presenten alarmas se muestra un cluster de color rojo
                    - markers       : Agrupa todos los Udat y se muestra un cluster de color verde
                    - markers2      : Agrupa todos las Subestacions  que presenten alarmas se muestra un cluster de color amarillo
                    - markers3      : Agrupa todos los reconectadores y se muestra un cluster de color verde
                
                Se inicializan los iconos de cada cluster mediante L.icon, aquí se puede personalizar el símbolo que tienen los item pertenecientes a cada cluster
                
                Se utiliza 
                
                    setInterval(function () { Actualizar(map, markersAlarms, markers, markers2, markers3, Sub, Aux, rc); }, 3000);
                    
                llama la funcion Actualizar() cada 3000 ms y le entrega el mapa, los clusters y los iconos. esto permite actualizar los clusters y alarmas cada 3 segundos.
                
#### Función CargarMapa()            
                        <script type="text/javascript">       
                        var a=0;
                           function CargarMapa() {

                                    L.mapbox.accessToken = 'pk.eyJ1IjoiY2FkaWF6dCIsImEiOiJjajV5OHRiMXIwMGk1MnducWIwYTI4ZzFkIn0.euMyiXXuemDcg8I9CrDjsw';
                                    
                                    var map = L.mapbox.map('map', 'mapbox.streets').setView([-23.82, -70.415], 7);
                                    var markersAlarms = new L.MarkerClusterGroup({
                                        iconCreateFunction: function (cluster) { return new L.DivIcon({ html: '<div><span>' + cluster.getChildCount() + '</span></div>', className: 'leaflet-marker-icon marker-cluster marker-cluster-large leaflet-zoom-animated leaflet-interactive', iconSize: new L.Point(40, 40) }); },
                                        disableClusteringAtZoom: 10
                                    });
                                    var markers = new L.MarkerClusterGroup({
                                        iconCreateFunction: function (cluster) { return new L.DivIcon({ html: '<div><span>' + cluster.getChildCount() + '</span></div>', className: 'leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive', iconSize: new L.Point(40, 40) }); },
                                        disableClusteringAtZoom: 10
                                    });
                                    var markers2 = new L.MarkerClusterGroup({
                                        iconCreateFunction: function (cluster) { return new L.DivIcon({ html: '<div><span>' + cluster.getChildCount() + '</span></div>', className: 'leaflet-marker-icon marker-cluster marker-cluster-medium leaflet-zoom-animated leaflet-interactive', iconSize: new L.Point(40, 40) }); },
                                        disableClusteringAtZoom: 10
                                    });
                                    var markers3 = new L.MarkerClusterGroup({
                                        iconCreateFunction: function (cluster) { return new L.DivIcon({ html: '<div><span>' + cluster.getChildCount() + '</span></div>', className: 'leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive', iconSize: new L.Point(40, 40) }); },
                                        disableClusteringAtZoom: 10
                                    });
                                    
                                  
                        Con un click sobre cada elemto perteneciente a cada cluster se activa la funcion sendDataAjax()(Función presente en un script aparte)  
                                    
                                    markers.on('click', function (a) {
                                    sendDataAjax(a.layer.getPopup().getContent(), a.layer.getPopup().getContent());
                                    });
                                    
                                    markers2.on('click', function (a) {
                                    sendDataAjax(a.layer.getPopup().getContent(), a.layer.getPopup().getContent());
                                    });
                                    
                                    markers3.on('click', function (a) {
                                    sendDataAjax(a.layer.getPopup().getContent(), a.layer.getPopup().getContent());
                                    });
                                    
                                    markersAlarms.on('click', function (a) {
                                    sendDataAjax(a.layer.getPopup().getContent(), a.layer.getPopup().getContent());
                                    });

                        la funcion sendDataAjax() llama a la funcion GetDataAjax()(Presente en el Back end), esta funcion realiza la escritura por OPC del nombre del item presionado en el mapa en la PDB de iFix
                        
                                    <script type="text/javascript">  
                                            function sendDataAjax(nombre, apellido) {
                                                var actionData = "{'nombre': '" + nombre + "','apellido': '" + apellido + "' } ";
                                            $.ajax(
                                                {
                                                    url: "mapa.aspx/GetDataAjax",
                                                    data: actionData,
                                                    dataType: "json",
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    success: function (msg) {  },
                                                    error: function (result) {
                                                    alert("ERROR " + result.status + ' ' + result.statusText);
                                                    }
                                                });        
                                            };
                                    </script>
                                    
                                    
                    Se definen los iconos para cada cluster
                    
                                    var Sub = L.icon({
                                        iconUrl: 'img/SE.png',
                                        iconSize: [35, 35], // size of the icon
                                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                                    });
                                    var Aux = L.icon({
                                        iconUrl: 'img/alerta.png',
                                        iconSize: [35, 35], // size of the icon
                                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                                    });
                                    var rc = L.icon({
                                        iconUrl: 'img/recloser.png',
                                        iconSize: [35, 35], // size of the icon
                                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                                    });
                        
                                    
                                    setInterval(function () { Actualizar(map, markersAlarms, markers, markers2, markers3, Sub, Aux, rc); }, 3000);
                                }
                                
#### Función Actualizar()                                
                                
                        La función actualizar realiza una llamada ajax para cada tipo de item y mediante las funciones :
                            
                            -   GetSubstation. 
                            -   GetUdat. 
                            -   GetRecloser.
                        
                        Obtiene una lista de objetos desde el Back end para cada tipo, cada objeto tiene:
                        
                            -   latitud.
                            -   Longitud.
                            -   Nombre.
                            -   Alarma.
                        
                        Para cada item. 
                        Finalmente si la consulta Ajax fue exitosa se realiza una actualización de los clusters mostrados en el mapa.
                        
                        
            
                                function Actualizar(map, markersAlarms, markers, markers2, markers3, Sub, Aux, rc) {
                                    
                        
                                    a = a + 1;
                                    
                                    $.ajax({
                                        type: "POST",
                                        url: "Trobjetos.aspx/GetSubstation",
                                        data: "{}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (response) {
                                            var subestacion = (typeof response.d) == 'string' ?
                                                eval('(' + response.d + ')') :
                                                response.d;
                                            var marker2;
                                            markers2.clearLayers();
                                            for (var i = 0; i < subestacion.length; i++) {
                                                if (subestacion[i].Alarm == "False") {
                                                    var marker2 = L.marker(new L.LatLng(subestacion[i].Lat, subestacion[i].Long), {
                                                        title: subestacion[i].Name,
                                                        icon: Sub
                                                    });
                                                }
                                                else {
                                                    var marker2 = L.marker(new L.LatLng(subestacion[i].Lat, subestacion[i].Long), {
                                                        title: subestacion[i].Name
                                                    });
                                                }
                                                var titulo = subestacion[i].Name;
                                                marker2.bindPopup(titulo);
                                                markers2.addLayer(marker2);
                                            }
                        
                        
                                            map.addLayer(markers2);
                        
                        
                                        },
                                        error: function (result) {
                                            alert('ERROR ' + result.status + ' ' + result.statusText);
                                        }
                                    });
                        
                                    //////
                                    //////
                                    //////
                        
                        
                                    $.ajax({
                                        type: "POST",
                                        url: "Trobjetos.aspx/GetUdat",
                                        data: "{}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (response) {
                                            var Udat = (typeof response.d) == 'string' ?
                                                eval('(' + response.d + ')') :
                                                response.d;
                        
                                            var marker;
                                            markersAlarms.clearLayers();
                                            markers.clearLayers();
                                            for (var i = 0; i < Udat.length; i++) {
                                                if (Udat[i].Alarm == "True") {
                                                    var marker = L.marker(new L.LatLng(Udat[i].Lat, Udat[i].Long), {
                                                        title: Udat[i].Name,
                                                        icon: Aux
                                                    });
                                                    var titulo = Udat[i].Name;
                                                    marker.bindPopup(titulo);
                                                    markersAlarms.addLayer(marker);
                                                }
                                                else {
                                                    var marker = L.marker(new L.LatLng(Udat[i].Lat, Udat[i].Long), {
                                                        title: Udat[i].Name
                                                    });
                                                    var titulo = Udat[i].Name;
                                                    marker.bindPopup(titulo);
                                                    markers.addLayer(marker);
                                                }
                        
                                            }
                                            map.addLayer(markers);
                        
                                            //map.addLayer(markers);
                                            //map.addLayer(markersAlarms);
                        
                                        },
                                        error: function (result) {
                                            alert('ERROR ' + result.status + ' ' + result.statusText);
                                        }
                                    });
                        
                                    //////
                                    //////
                                    //////
                        
                        
                                    $.ajax({
                                        type: "POST",
                                        url: "Trobjetos.aspx/GetRecloser",
                                        data: "{}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (response) {
                                            var Recloser = (typeof response.d) == 'string' ?
                                                eval('(' + response.d + ')') :
                                                response.d;
                                            var marker;
                                            markers3.clearLayers();
                                            for (var i = 0; i < Recloser.length; i++) {
                                                if (Recloser[i].Alarm == "True") {
                                                    var marker = L.marker(new L.LatLng(Recloser[i].Lat, Recloser[i].Long), {
                                                        title: Recloser[i].Name,
                                                        icon: Aux
                                                    });
                                                    var titulo = Recloser[i].Name;
                                                    marker.bindPopup(titulo);
                                                    markers3.addLayer(marker);
                                                }
                                                else {
                                                    var marker = L.marker(new L.LatLng(Recloser[i].Lat, Recloser[i].Long), {
                                                        title: Recloser[i].Name,
                                                        icon: rc
                                                    });
                                                    var titulo = Recloser[i].Name;
                                                    marker.bindPopup(titulo);
                                                    markers3.addLayer(marker);
                                                }
                                            }
                                            
                                            map.addLayer(markers3);
                                            map.addLayer(markersAlarms);
                                        },
                                        error: function (result) {
                                            alert('ERROR ' + result.status + ' ' + result.statusText);
                                        }
                                    });
                                    ////
                                    ////
                                    ////
                                 
                                }
                              
                            </script>
                            
                            
# - Back end. 
            - La programación del back end se basa en C#.    
            - Implementa el cliente OPC TitaniumAS.OPC.Client : Permite la lectura y escritura de datos por OPC
                
### Definición de nodo

Cada item se asocia como un nodo con las propiedades latitud, longitud, nombre y estado de alarma

        public class Nodo
        {
            public string Lat { get; set; }
            public string Long { get; set; }
            public string Name { get; set; }
            public string Alarm{ get; set; }
        }
       
### Función GetUdat, GetSubstation y GetRecloser

        Implementa los Metodos GetUdat, GetSubstation, GetRecloser donde se define la lista de objetos correspondiente de cada item 
        como ejemplo se muestra GetUdat() donde el campo Alarm llama a la funcion GetAlarms donde se setea como True o False el valor de alarma 
        
                    [WebMethod]
                    public static List<Nodo> GetUdat()
                    {
                        return new List<Nodo>
                        {
                            new Nodo { Lat="-23.1067", Long="-70.4477", Name="UDAT 5-252", Alarm =GetAlarms()},
                            new Nodo { Lat="-25.4006", Long="-70.4736", Name="UDAT 6-16", Alarm ="False"},
                        };
                    }

### Función GetAlarms()

        Obtiene el estado de alarma de cada item  mediante la lectura por OPC del OPC Server de iFix
        
                public static string GetAlarms()
                {
                    Uri url = UrlBuilder.Build("Matrikon.OPC.Simulation.1");
                    using (var server = new OpcDaServer(url))
                    {
                        // Connect to the server first.
                        server.Connect();
                        var browser = new OpcDaBrowserAuto(server);
                        // BrowseChildren(browser);
                        OpcDaGroup group = server.AddGroup("Group0");
                        group.IsActive = true;
                        var definition1 = new OpcDaItemDefinition
                        {
                            ItemId = "Bucket Brigade.Boolean",
                            IsActive = true
                        };
                        var definition2 = new OpcDaItemDefinition
                        {
                            ItemId = "Random.Boolean",
                            IsActive = true
                        };
                        OpcDaItemDefinition[] definitions = { definition1, definition2 };
                        OpcDaItemResult[] results = group.AddItems(definitions);
                        foreach (OpcDaItemResult result in results)
                        {
                            if (result.Error.Failed)
                                Console.WriteLine("Error adding items: {0}", result.Error);
                        }
                        OpcDaItemValue[] values = group.Read(group.Items, OpcDaDataSource.Device);
                        //  OpcDaItemValue[] values = await group.ReadAsync(group.Items);
                        string a;
                        if (values[0].Value.ToString() == "True") { a = "True"; }
                        else { a =  GetAlarms(); }
                        return a;
                    }
                }
        

        
### Función GetDataAjax()
        
        Esta función es llamada desde el Front end mediante una llamada Ajax. Envía el nombre del item presionado mediante OPC, modifica el valor del Tag en la PDB asociado al picture presente en workspace.
        
                    [WebMethod]
                    public static string GetDataAjax(string nombre, string apellido)
                    {
                        Uri url = UrlBuilder.Build("Matrikon.OPC.Simulation.1");
                        using (var server = new OpcDaServer(url))
                        {
                            // Connect to the server first.
                            server.Connect();
                            var browser = new OpcDaBrowserAuto(server);
                            // BrowseChildren(browser);
                            OpcDaGroup group = server.AddGroup("Group0");
                            group.IsActive = true;
                            var definition1 = new OpcDaItemDefinition
                            {
                                ItemId = "Bucket Brigade.String",
                                IsActive = true
                            };
                            var definition2 = new OpcDaItemDefinition
                            {
                                ItemId = "Bucket Brigade.String",
                                IsActive = true
                            };
                            OpcDaItemDefinition[] definitions = { definition1, definition2 };
                            OpcDaItemResult[] results = group.AddItems(definitions);
                            // Handle adding results.
                            foreach (OpcDaItemResult result in results)
                            {
                                if (result.Error.Failed)
                                    Console.WriteLine("Error adding items: {0}", result.Error);
                            }
                            OpcDaItem int2 = group.Items.FirstOrDefault(i => i.ItemId == "Bucket Brigade.String");
                            OpcDaItem int4 = group.Items.FirstOrDefault(i => i.ItemId == "Bucket Brigade.String");
                            OpcDaItem[] items = { int2, int4 };
                            // Write values to the items synchronously.
                            object[] values2 = { nombre, apellido };
                            HRESULT[] results2 = group.Write(items, values2);
                        }
                        return string.Format("Bienvenido al mundo AJAX {0} {1}", nombre, apellido);
                    }
               
